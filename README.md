This project contains the scripts used to create git mirrors from LHCb SVN hosted projects.

See https://its.cern.ch/jira/browse/LBCORE-729 for details.

Create a git svn mirror
-----------------------
1. Set the variable `project` to the project name:
  ```
  project=<Project>
  ```
2. from a temporary directory call
  ```
  /afs/cern.ch/lhcb/software/GIT/svn-gateways/update_lhcb_project.sh $project
  ```
  (afterwards the directory `${project}.git` can be removed)
3. check that the trunk and the result of getpack are equivalent:
  ```
  getpack -PH -p anonymous $project
  svn co http://svn.cern.ch/guest/lhcb/${project}/trunk ${project^^}/${project^^}_trunk
  diff -r -q --exclude .svn --exclude version.cmt ${project^^}/${project^^}_{HEAD,trunk}
  ```
  inconsistencies must be fixed before proceeding.
4. [create a git project](https://gitlab.cern.ch/projects/new?namespace_id=276) in https://gitlab.cern.ch/LHCb-SVN-mirrors
  * import URL: `http://cern.ch/lhcbproject/GIT/svn-mirrors/<Project>.git`
  * description: Git mirror of `<Project>` project on SVN.
  * visibility: Public
5. when the import is completed, run
  ```
  /afs/cern.ch/lhcb/software/GIT/svn-gateways/link_to_svn.sh $project
  ```
6. run
  ```
  /afs/cern.ch/lhcb/software/GIT/svn-mirrors/link_to_gitlab.sh $project
  ```
7. make the `master` branch protected in the project settings on gitlab