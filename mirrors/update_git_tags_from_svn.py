#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
import os
import re
from subprocess import check_call, check_output, call, STDOUT, CalledProcessError
from os.path import join

from xml.etree import ElementTree as ET
from StringIO import StringIO
import shutil

GIT_URL_TPL = 'ssh://git@gitlab.cern.ch:7999/LHCb-SVN-mirrors/{}.git'
SVN_URL_TPL = 'http://svn.cern.ch/guest/lhcb/{}'

_VK_RE = re.compile(r'(\d+|\D+)')
def versionKey(x):
    '''
    Key function to be passes to list.sort() to sort strings
    as version numbers.
    '''
    return [int(i) if i[0] in '0123456789' else i
            for i in _VK_RE.split(x)
            if i]

def svn_tag_url(project):
    return SVN_URL_TPL.format(project) + '/tags/' + project.upper()

def git_clone(project):
    if not os.path.exists(project):
        print('Cloning {} from gitlab'.format(project))
        check_call(['git', 'clone', GIT_URL_TPL.format(project)])
    else:
        print('Updating {} from gitlab'.format(project))
        check_call(['git', 'checkout', '-f', 'master'], cwd=project)
        check_call(['git', 'clean', '-xdf'], cwd=project)
        check_call(['git', 'pull'], cwd=project)

def get_svn_tags(project):
    print('Getting {} tags from svn'.format(project))
    out = check_output(['svn', 'ls', svn_tag_url(project)])
    tag_exp = re.compile(r'{}_(v\d+r\d+.*)/'.format(project.upper()))
    tags = [m.group(1) for m in map(tag_exp.match, out.split()) if m]
    return tags

def get_git_tags(project):
    print('Getting {} tags from git'.format(project))
    check_call(['git', 'fetch', '--tags'], cwd=project)
    out = check_output(['git', 'tag'], cwd=project)
    tags = out.split()
    return tags

def missing_tags(project):
    tags = list(set(get_svn_tags(project)) - set(get_git_tags(project)))
    tags.sort(key=versionKey)
    print('Found {} missing tags: {}'.format(len(tags), tags))
    return tags

def getpack(project, version):
    base = join(project.upper(), '{}_{}'.format(project.upper(), version))
    if not os.path.exists(base):
        print('Checkout {} {} from svn'.format(project, version))
        check_output(['getpack', '--protocol=anonymous',
                      '--project', '--recursive',
                      '--plain', '--batch', '--force', project, version])

def svn_last_commit(path):
    xml = check_output(['svn', 'info', '--xml', path])
    tree = ET.parse(StringIO(xml))
    return int(tree.find('entry/commit').attrib['revision'])

def svn_revison_candidate(project, version):
    base = join(project.upper(), '{}_{}'.format(project.upper(), version))
    cand = max(svn_last_commit(path)
               for path in [join(base, 'CMakeLists.txt'),
                            join(base, 'cmt', 'project.cmt'),
                            join(base, project + 'Sys', 'CMakeLists.txt'),
                            join(base, project + 'Sys', 'cmt', 'requirements'),
                            join(base, project + 'Sys', 'doc', 'release.notes')]
               if os.path.exists(path))
    print('Revision {} is a candidate for {} {}'.format(cand, project, version))
    return cand

def git_commit_candidate(project, svn_revision):
    print('Looking for git commit matching revision {}'.format(svn_revision))
    commits = check_output(['git', 'log', '-z', '--pretty=format:%H%n%b', 'master'],
                           cwd=project).split('\0')
    for commit in commits:
        commit_id, message = commit.split('\n', 1)
        m = re.search(r'git-svn-id: .*@(\d+) ', message)
        if m and int(m.group(1)) <= svn_revision:
            print('Using {}'.format(commit_id))
            return commit_id
    print('Cannot find git candidate for svn revision %d' % svn_revision)
    return None


def git_tag_candidate(project, version):
    print('Looking for git commit usable for tag {}'.format(version))
    v_key = versionKey(version)
    tags = [t for t in sorted(get_git_tags(project), key=versionKey)
            if versionKey(t) < v_key]
    if tags:
        print('Using {}'.format(tags[-1]))
        return tags[-1]
    print('Cannot find git candidate for svn tag %s' % version)
    return None

KEYWORD_EXP = re.compile(r'\$ *(Author|CVSHeader|Header|Id|Name|Locker|RCSfile|Source|State|(Head)?URL|(LastChanged)?(Date|Rev(ision)?)|LastChangedBy)[^$]*\$')

def equivalent(src, dst):
    if os.path.exists(dst):
        with open(src) as lines:
            src_content = [KEYWORD_EXP.sub(r'$\1$', l) for l in lines]
        with open(dst) as lines:
            dst_content = [KEYWORD_EXP.sub(r'$\1$', l) for l in lines]
        return src_content == dst_content
    return False

def sync_git_with_svn(project, version):
    print('Synchronizing git work dir with svn checkout...')
    copied = 0
    removed = 0
    svn_base = join(project.upper(), '{}_{}'.format(project.upper(), version))
    # copy files from svn to git
    for root, dirs, files in os.walk(svn_base):
        if '.svn' in dirs:
            dirs.remove('.svn')
        for f in files:
            src = join(root, f)
            dst = join(project, os.path.relpath(src, svn_base))
            if not equivalent(src, dst):
                if not os.path.exists(os.path.dirname(dst)):
                    os.makedirs(os.path.dirname(dst))
                shutil.copy(src, dst)
                check_call(['git', 'add', os.path.relpath(dst, project)],
                           cwd=project)
                copied += 1
    # remove files in git that do not exist in svn
    for root, dirs, files in os.walk(project):
        if '.git' in dirs:
            dirs.remove('.git')
        for f in files:
            dst = join(root, f)
            src = join(svn_base, os.path.relpath(dst, project))
            if not os.path.exists(src):
                check_call(['git', 'rm', os.path.relpath(dst, project)],
                           cwd=project)
                removed += 1
    print('{} changes ({} files copied, {} files removed)'.format(copied + removed, copied, removed))

def update_git_tags(project):
    git_clone(project)
    for version in missing_tags(project):
        try:
            getpack(project, version)
            commit = (git_commit_candidate(project,
                                           svn_revison_candidate(project, version))
                      or git_tag_candidate(project, version))
            if not commit:
                print('Cannot find git candidate')
                continue
            check_output(['git', 'checkout', commit], cwd=project, stderr=STDOUT)
            sync_git_with_svn(project, version)
            call(['git', 'commit',
                  '-m', 'updated to match {} tag in svn'.format(version)],
                 cwd=project)
            check_call(['git', 'tag', version], cwd=project)
        except CalledProcessError, x:
            print('{0}: ignoring version {1}'.format(x, version))
            check_output(['git', 'reset', '--hard', 'HEAD'], cwd=project, stderr=STDOUT)
            check_output(['git', 'clean', '-xdf'], cwd=project, stderr=STDOUT)

    print('Now you can check "{}" and call "git push --tags origin"'.format(project))

if __name__ == '__main__':
    if len(sys.argv) == 2:
        update_git_tags(sys.argv[1])
    else:
        print('Usage: {} <project>'.format(os.path.basename(sys.argv[0])))
