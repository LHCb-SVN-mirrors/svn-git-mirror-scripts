#!/bin/bash -e

# This links the repository for a project with the gitlab mirror.

if [ $# != 1 ] ; then
  echo "Usage: $0 <project>"
  exit 1
fi

project=$1

basedir=$(dirname $0)
cd $basedir

if [ ! -d ${project}.git ] ; then
  echo error: no repository for $project
  exit 1
fi

cd ${project}.git
if git remote | grep -q gitlab ; then
  echo $project already linked to gitlab
else
  git remote add --mirror gitlab ssh://git@gitlab.cern.ch:7999/LHCb-SVN-mirrors/${project}.git
  git fetch gitlab
  git update-server-info
fi
