#!/bin/bash -e

# This script updates the git mirrors gateways with the changes coming from svn
# and pushes them to the official git mirror.

basedir=$(dirname $0)
cd $basedir
pidfile=pid.$(basename $0).$(hostname).$$
touch $pidfile
echo "==== started at $(date) ===="
for gateway in *.git ; do
  (
    echo Processing $gateway
    cd $gateway
    if git remote | grep -q gitlab ; then
      git fetch gitlab
      git update-server-info
    else
      echo $gateway not in gitlab
    fi
  )
done
rm -f $pidfile
echo "==== completed at $(date) ===="
