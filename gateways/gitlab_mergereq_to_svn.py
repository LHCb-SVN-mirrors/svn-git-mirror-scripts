#!/usr/bin/env python

import sys
import os
import re
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                             os.pardir, 'python'))
sys.path.append('/afs/cern.ch/lhcb/software/TagCollector/TagCollector')

import gitlab
from subprocess import call, check_call, CalledProcessError

try:
    from subprocess import check_output
except ImportError:
    def check_output(*popenargs, **kwargs):
        'backported from 2.7'
        from subprocess import Popen, PIPE
        if 'stdout' in kwargs:
            raise ValueError('stdout argument not allowed, it will be overridden.')
        process = Popen(stdout=PIPE, *popenargs, **kwargs)
        output, unused_err = process.communicate()
        retcode = process.poll()
        if retcode:
            cmd = kwargs.get("args")
            if cmd is None:
                cmd = popenargs[0]
            raise CalledProcessError(retcode, cmd)
        return output

token_file = os.path.expanduser(os.path.join('~', 'private', 'gitlab_token.txt'))
token = os.environ.get('GITLAB_TOKEN') or open(token_file).read().strip()

server = gitlab.Gitlab(host='https://gitlab.cern.ch/', token=token)

_allProjects = []

def allProjects():
    global _allProjects
    if _allProjects:
        for p in _allProjects:
            yield p
    else:
        for p in server.getall(server.getprojects):
            if p['namespace']['name'] == 'LHCb-SVN-mirrors':
                _allProjects.append(p)
                yield p

class UnknownProject(RuntimeError):
    pass

def getProject(name_or_id):
    if isinstance(name_or_id, int):
        p = server.getproject(name_or_id)
        if p:
            return p
        else:
            raise UnknownProject('unknown project id %s' % name_or_id)
    else:
        for p in allProjects():
            if name_or_id == p['name']:
                return p
        raise UnknownProject('unknown project %s' % name_or_id)


def getProjectId(name):
    try:
        return int(name)
    except ValueError:
        pass
    name = 'LHCb-SVN-mirrors/%s' % name
    for p in allProjects():
        if p['path_with_namespace'] == name:
            return int(p['id'])
    raise UnknownProject('unknown project %s' % name)

def mergeRequests(project, state='opened', branch='master'):
    projectId = getProjectId(project)
    for mr in server.getall(server.getmergerequests, project_id=projectId, state=state):
        if mr['target_branch'] == branch:
            yield mr

def getProjectURL(project):
    if isinstance(project, (int, basestring)):
        project = getProject(project)
    return project['ssh_url_to_repo']


def fix_description(desc):
    '''
    Fix a description by

      - removing heading empty lines
      - removing trailing spaces
      - ensuring a single empty line at the end
    '''
    from itertools import dropwhile
    if desc:
        desc = '\n'.join(dropwhile(lambda l: not l,
                                   (l.rstrip() for l in desc.rstrip().splitlines()))) + '\n\n'
    return desc

MERGE_COMMIT_MSG_TPL = '''{title}

{description}Merge branch '{source_branch}' of {repo}

See merge request !{iid}
'''

def apply_merge_request(mr):
    '''
    Try to apply a merge request from the current directory.
    '''
    if mr['work_in_progress']:
        print 'Ignoring MR {id}: {title}'.format(**mr)
        return
    print 'Applying MR {id}: {title}'.format(**mr)
    mr_url = '{url}/merge_requests/{id}'.format(url=getProject(mr['target_project_id'])['web_url'],
                                                id=mr['iid'])
    print '   ', mr_url
    src_url, src_branch = getProjectURL(mr['source_project_id']), mr['source_branch']
    src_project_name = getProject(mr['source_project_id'])['path_with_namespace']

    server.updatemergerequest(mr['target_project_id'], mr['id'],
                              assignee_id=server.currentuser()['id'])

    try:
        check_call(['git', 'checkout', 'master'])
        check_call(['git', 'fetch', src_url, src_branch])
        msg = MERGE_COMMIT_MSG_TPL.format(repo=src_project_name, **mr)

        check_call(['git', 'merge', '--no-ff', '-m', msg, 'FETCH_HEAD'])
        check_call(['git', 'svn', 'dcommit'])
        check_call(['git', 'push'])
        return True
    except CalledProcessError, exc:
        print exc
        call(['git', 'reset', '--hard', 'HEAD'])
    return False

def get_packages(proj_dir=os.curdir):
    all_packages = []
    for dirpath, dirnames, filenames in os.walk(proj_dir):
        if dirpath != proj_dir and 'CMakeLists.txt' in filenames:
            dirnames[:] = [] # do not recurse
            all_packages.append(os.path.relpath(dirpath, proj_dir))
    all_packages.sort()
    return all_packages

def get_svn_revision():
    match = re.search(r'git-svn-id: \S+@(\d+)', check_output(['git', 'log', '-1']))
    return int(match.group(1)) if match else None

def versionKey(v):
    '''
    For a version string with numbers alternated by alphanumeric separators,
    return a tuple containing the separators and the numbers.

    For example:
    >>> versionKey('1.2.3')
    (1, '.', 2, '.', 3)
    >>> versionKey('v10r0')
    ('v', 10, 'r', 0)
    >>> versionKey('1.2-a')
    (1, '.', 2, '-a')
    '''
    v = re.findall(r'[-a-zA-Z_.]+|\d+', v)
    return tuple([int(x) if x.isdigit() else x for x in v])

def update_tag_collector(project, mr):
    try:
        from TagCollector import TagCollector, ProjectTags
    except ImportError:
        print 'WARNING: cannot update tag collector (missing TagCollector API)'
        return

    revision = get_svn_revision()
    if not revision:
        print 'WARNING: cannot update tag collector (missing svn revision)'
        return

    tc = TagCollector()
    versions = [(tag.version, tag.id)
                for tag in tc.store.find(ProjectTags,
                                         ProjectTags.project==unicode(project),
                                         ProjectTags.hidden==0)]
    versions.sort(key=lambda v: versionKey(v[0]))
    if not versions:
        print 'WARNING: no open version for', project
        return

    changes = [l.split()[2]
               for l in check_output(['git', 'diff', '--numstat', 'HEAD~..HEAD']).splitlines()]
    def is_affected(pkg):
        pkg = os.path.join(pkg, '')
        for change in changes:
            if change.startswith(pkg):
                return True
        return False
    changed_packages = filter(is_affected, get_packages())

    level = 'minor'
    author = mr['author']['username']
    list_of_projects = [versions[-1][1]]
    mr_url = '{url}/merge_requests/{id}'.format(url=getProject(mr['target_project_id'])['web_url'],
                                                id=mr['iid'])
    comment = '''{title} (<a href="{mr_url}">mr !{iid}</a>)'''.format(mr_url=mr_url, **mr)
    for package in changed_packages:
        tc.addLocalRevision(package, 'r%d' % revision, level, author,
                            list_of_projects, comment)


GW_REPO_BASE = os.path.abspath(os.path.dirname(__file__))

def main():
    from datetime import datetime
    print '--- started', datetime.now()

    for project in allProjects():
        try:
            gateway_dir = os.path.join(GW_REPO_BASE, '{name}-git-svn'.format(**project))
            if not os.path.exists(gateway_dir):
                continue
            os.chdir(gateway_dir)

            for mr in reversed(list(mergeRequests(project['id']))):
                mr['description'] = fix_description(mr['description'])
                if apply_merge_request(mr):
                    update_tag_collector(project['name'], mr)
        except Exception, x:
            print 'error: exception in %s: %s' % (project['name'], x)
    print '--- completed', datetime.now()


if __name__ == '__main__':
    main()
