#!/bin/bash -e

# This script generates a git repository that mirrors the history of releases
# of an LHCb project.
# If the git repository already exists, then it is updated with new tags.

if [ $# != 1 ] ; then
  echo "Usage: $0 <project>"
  exit 1
fi

project=$1

base_git_url=/afs/cern.ch/lhcb/software/GIT/svn-mirrors
branch=master
base_svn_url=http://svn.cern.ch/guest/lhcb

if [ ! -e ${base_git_url}/${project}.git ] ; then
  git init --bare ${base_git_url}/${project}.git
  mv ${base_git_url}/${project}.git/hooks/post-update.sample ${base_git_url}/${project}.git/hooks/post-update
fi

if [ ! -e $project ] ; then
  git clone -b ${branch} ${base_git_url}/${project}.git
fi

latest_version=$(cd ${project} && (git tag | sort -V | tail -1))
if [ -n "${latest_version}" ] ; then
  echo "Latest version in git: $latest_version"
  new_versions=$(svn ls ${base_svn_url}/${project}/tags/${project^^} | grep ^${project^^}_v | sed s/^${project^^}_//';'s_/__ | sort -V | awk "{if(f)print}/${latest_version}/{f=1}" | xargs)
else
  echo "No version in git"
  new_versions=$(svn ls ${base_svn_url}/${project}/tags/${project^^} | grep ^${project^^}_v | sed s/^${project^^}_//';'s_/__ | sort -V | xargs)
fi
if [ -z "$new_versions" ] ; then
  echo "Nothing to add"
  exit 0
fi

echo "Versions to add: $new_versions"

for version in ${new_versions} ; do
  if [ ! -e $LHCBRELEASES/${project^^}/${project^^}_${version} ] ; then
    echo "WARNING: version $version of $project is not in the release area"
  fi
  echo "===> checking out $project $version ($(date))"
  echo "==================== $project $version ====================" >> ${project}.getpack.log
  echo "==================== $project $version ====================" >> ${project}.git.log
  rm -rf ${project^^}/${project^^}_${version}

  if getpack --protocol anonymous --project --recursive --export --plain --batch ${project} ${version} &>> ${project}.getpack.log ; then

    rm -rf ${project}/*
    mv ${project^^}/${project^^}_${version}/* ${project}/

    (
      cd ${project}
      git add -f .
      git commit --allow-empty -a -m ${version}
      git tag ${version}
    ) &>> ${project}.git.log

  else
    echo "WARNING: getpack of $project $version failed, ignoring it"
    rm -rf ${project^^}/${project^^}_${version}
  fi
done

echo "==================== publishing ====================" >> ${project}.git.log
(
  cd ${project}
  git push origin master >> ${project}.git.log
  git push --tags >> ${project}.git.log
)
