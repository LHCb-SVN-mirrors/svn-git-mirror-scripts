#!/bin/bash -e

# This script takes a git repository prepared with "update_lhcb_project.sh"
# and connects its HEAD version with the current trunk in SVN so that new changes
# can be automatically managed with git-svn.
#
# Note that once the link is established, the local repository must be used as
# gateway to copy changes from svn to git.

if [ $# != 1 ] ; then
  echo "Usage: $0 <project>"
  exit 1
fi

basedir=$(dirname $0)
cd $basedir

project=$1

url=svn+ssh://svn.cern.ch/reps/lhcb/${project}
rev=$(LC_ALL=C svn info $url/trunk | awk '/^Last Changed Rev:/{print $NF}')

if [ ! -e ${project}-git-svn ] ; then
  #git clone /afs/cern.ch/lhcb/software/GIT/svn-mirrors/${project}.git ${project}-git-svn
  git clone ssh://git@gitlab.cern.ch:7999/LHCb-SVN-mirrors/${project}.git ${project}-git-svn
fi

(
  cd ${project}-git-svn
  git svn init $url/trunk
  cp .git/refs/heads/master .git/refs/remotes/git-svn
  git svn fetch -r $rev
  git checkout master
  git merge remotes/git-svn
)

cat <<EOF
To update the git repository from svn:

  cd $(pwd)/${project}-git-svn
  git svn fetch
  git checkout master
  git merge remotes/git-svn
  git push

EOF
