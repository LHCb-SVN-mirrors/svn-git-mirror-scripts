#!/bin/bash

# This script updates the git mirrors gateways with the changes coming from svn
# and pushes them to the official git mirror.

basedir=$(dirname $0)
cd $basedir
pidfile=$(basename $0).$(hostname).$$.pid
touch $pidfile
echo "==== started at $(date) ($(hostname).$$) ===="
for gateway in *-git-svn ; do
  (
    echo Processing $gateway
    cd $gateway
    git svn fetch --use-log-author
    git checkout master
    git merge remotes/git-svn
    git push
  )
done
rm -f $pidfile
echo "==== completed at $(date) ($(hostname).$$) ===="
